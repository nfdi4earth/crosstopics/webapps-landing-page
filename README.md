# Landing Pages

This project administrates all the central landing pages that need to be hosted for the services of [NFDI4Earth](https://nfdi4earth.de).

## Deployment

CI/CD pipelines are used to automatically deploy the latest state to
the virtual machine (support-apps).

### VM-preparation

The VM needs to prepared to be able to get accessed via CI/CD-pipeline. The whole action is covered by dedicated [ansible scripts](https://git.rwth-aachen.de/nfdi4earth/architecture/server-administration/-/blob/master/roles/supportapps/tasks/landing_pages.yaml?ref_type=heads) which contains all the necessary steps:

- create dedicated user 'user4lp'
- prepare ssh for that user
- set permissions and ownership of `/var/www` to enable 'user4lp' to handle git project
- clone this git project to `/var/www` and adapt permissions/ownership

## Branches:

Currently, we develop on 2 main branches:

- master: used to develop and stage the project
    
    -> commits get deployed to the [Staging Version](https://stg-landing-pages.support-apps.n4e.geo.tu-dresden.de)

- production: used to maintain the productive version of all landing pages
    
    -> commits get deployed to the [Production Version](https://landing-pages.support-apps.n4e.geo.tu-dresden.de)

> Note: Please make sure to fully test landing pages after pushing new features before merging them to the production branch!

### The pipeline

When the VM is set up well, the pipeline will run on new commits for both branches, as defined in: [`.gitlab-ci.yml`](.gitlab-ci.yml).

> Info: This script is fundamentally based on a very good [tutorial](https://www.programonaut.com/how-to-deploy-a-git-repository-to-a-server-using-gitlab-ci-cd/)!

Basically, the integrated tasks are identical for both stages. Only ssh_host and git branches are different.

Short wrap up of what happens:

1. prepare ssh (within the docker container) to enable access to virtual machine
2.  - connect to virtual machine
    - go to workdir of landing page
    - discard local changes
    - checkout corresponding branch
    - pull updates
    - adapt ownership of workdir
    - exit VM
3. remove ssh setting (within docker container)

## License

This project is published under the Apache License 2.0, see file LICENSE.
Contributors: Ralf Klammer