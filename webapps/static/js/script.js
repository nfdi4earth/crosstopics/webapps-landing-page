var renderCard = function (project) {
    // clone the template card
    let card = $("#card-template > div").clone();

    // fill the top of the card with the image
    card.find("img.card-img-top")
        .attr("src", project.image)
        .attr("alt", "Image showing a screenshot of the webapplication: " + project.title.long);

    // add badge pill
    badge = card.find(".badge")
    badge.text(project.status)
    if (project.status == "Finished") {
        badge.addClass("badge-success");
    } else {
        badge.addClass("badge-warning");
    }

    // add card title
    card.find(".card-title").text(project.title.long);

    card.find(".card-text").text(project.description);

    // add project url
    card.find(".project-link").attr('href', project.url);

    // add external links
    project.documents.forEach(function (document, i) {
        let link = $('<a data-type="text" target="_blank"></a>');
        link.text(document.title);
        link.attr('href', document.url);
        card.find('.card-footer .text-muted').append(link);
        if (i < project.documents.length - 1) {
            card.find('.card-footer .text-muted').append('<span class="mx-1">|</span>');
        }
    });

    // if (project.status in states) {
    //     card.find('.badge').addClass(states[project.status]);
    // };

    // append card to DOM
    card.appendTo($('#card-deck'));
};

// page is fully loaded, so functions can be executed now
$(function () {
    projects.forEach(renderCard);

    $('[data-toggle="tooltip"]').tooltip()

    console.log("script.js ready!");
});
