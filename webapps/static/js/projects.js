const projects = [
    {
        'title': {
            'long': 'Graph based Visual Search Engine',
            'short': 'VESA'
        },
        'image': 'static/portfolios/vesa.png',
        'description': 'VESA is a visualization-enabled search interface using a graph database to improve findability and multidimensional access in large, interconnected data repositories.',
        'url': 'https://vesa.webapps.nfdi4earth.de',
        'documents': [
            {
                'title': 'Pilot proposal',
                'url': 'https://www.nfdi4earth.de/images/379_VisualSearchMachine.pdf'
            },
            {
                'title': 'GitLab project',
                'url': 'https://git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/pilots/graph-based-visual-search-engine'
            },
        ],
        'status': 'ALPHA'
    },
]
