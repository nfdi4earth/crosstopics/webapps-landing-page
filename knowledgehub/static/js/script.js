const sparql_url = "https://sparql.knowledgehub.nfdi4earth.de";

var renderCard = function (data) {
    let description_parts = data.response
        .split('\n')
        .slice(0, 3)
        .filter(function (line) {
            return line.startsWith('#');
        });
    let description = description_parts
        .join('\n')
        .replace('# ', '')
        .replace('\n# ', '\n')
        .replace('#', '');
    let try_url = sparql_url + "?qtxt=" + encodeURIComponent(data.response),
        data_url = sparql_url + "/?query=" + encodeURIComponent(data.response),
        card = $("#card-template > div").clone();

    if (data && data.title) card.find(".card-header #card-header").text(data.title);
    else card.find(".card-header").hide();

    card.find("p.card-text").text(description);
    card.find("a.download").each(function (i, d) {
        var self = $(this);
        self.attr("href", data_url + "&format=" + self.data("type"));
    });
    card.find("a.card-button").attr("href", try_url);

    card.appendTo(data.card_deck);
};

var fetchGitLab = function (data) {
    fetch(data.url, { headers: { 'PRIVATE-TOKEN': 'glpat-5Fy-zjKzXW2wWojz9Rab' } })
        .then(function (response) {
            if (data != undefined && data.datatype === 'json') return response.json();
            else return response.text();
        })
        .then(function (response) {
            data.response = response;
            data.callback(data)
        })
        .catch(function (err) {
            // There was an error
            console.warn('Something went wrong.', err);
        });
}

var getQuestionaireQuery = function (filename) {
    fetchGitLab({
        callback: renderCard,
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/files/queries%2F' + filename + '/raw?ref=main',
        datatype: 'text',
        card_deck: $("#hidden-card-deck .row")
    })
};
var getQuestionaireList = function () {
    fetchGitLab({
        callback: function (data) {
            data.response.forEach(function (elem) {
                if (elem.type === 'blob') getQuestionaireQuery(elem.name);
            });
            // remove event from DOM, that triggers this function
            $('#hidden-card-deck').off('show.bs.collapse')
        },
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/tree?ref=main&path=queries&per_page=100',
        datatype: 'json'
    })
};

var getExampleQuery = function (filename) {
    fetchGitLab({
        callback: renderCard,
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/files/examples%2F' + filename + '/raw?ref=main',
        datatype: 'text',
        title: filename.replace('.rq', ''),
        card_deck: $("#card-deck")
    })
};
var getExamplesList = function () {
    fetchGitLab({
        callback: function (data) {
            data.response.forEach(function (elem) {
                if (elem.type === 'blob') getExampleQuery(elem.name);
            })
        },
        url: 'https://git.rwth-aachen.de/api/v4/projects/81019/repository/tree?ref=main&path=examples',
        datatype: 'json'
    })
};

var generateLink = function (href, text) {
    if (text === undefined) text = href;
    return `<a href="${href}" target="_blank">${text}</a>`;

}

var replaceLinks = function (content, link) {
    /**
    * Replaces links and the word "homepage" in the given content.
    * If a URL is found, it replaces it with a formatted link.
    * Similarly, if the word "homepage" is found, it replaces it with the
    * provided link.
    * @param {string} content - The content in which replacements should be made.
    * @param {string} link - The link to be used for replacing "homepage".
    * @returns {string} - The new content with replaced links.
    */
    const link_pattern = /https?:\/\/[^\s.]+(?:\.[^\s.]+)*/;
    const link_match = content.match(link_pattern);
    const homepage_pattern = /homepage/;
    const homepage_match = content.match(homepage_pattern);
    let new_content = content;
    if (link_match) {
        new_content = new_content.replace(link_pattern, generateLink(link_match[0]));
    }
    if (homepage_match) {
        new_content = new_content.replace(homepage_pattern, generateLink(link, 'homepage'));
    }
    return new_content;
}
function sortByServiceName(data) {

    /**
     * Sorts an array of objects by the `serviceName` property.
     * @param {Array} data - The array of objects to be sorted.
     * @returns {Array} - The sorted array.
     */
    return Object.values(data).sort((a, b) => {
        const nameA = a.serviceName ? a.serviceName.toLowerCase() : '';
        const nameB = b.serviceName ? b.serviceName.toLowerCase() : '';
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0;
    });
}


var showHarvestingResources = function () {
    const rev = 'dev';
    const filename = 'harvesters-config.json';
    const project_id = 75792;
    const url = 'https://git.rwth-aachen.de/api/v4/projects/';
    fetchGitLab({
        callback: function (data) {
            const table_body = $('#provider tbody');
            // Convert the response object to an array and sort it by serviceName
            const sortedData = sortByServiceName(data.response);

            for (var key in sortedData) {
                const d = sortedData[key];
                if (d.serviceName === undefined) {
                    continue;
                }
                const row = $('<tr class="row"></tr>');
                row.append('<th class="col-3 border">' + d.serviceName + '</th>');
                row.append('<td class="col-2 border">' + d.resourceTypes + '</td>');
                row.append('<td class="col-5 border">' + replaceLinks(d.description, d.serviceHomepage) + '</td>');
                row.append($('<td class="col-2 border"></td>').append(generateLink(d.serviceHomepage)));
                table_body.append(row)
            };
        },
        url: `${url}${project_id}/repository/files/kh_populator%2F${filename}/raw?ref=${rev}`,
        datatype: 'json'
    })
};

// page is fully loaded, so functions can be executed now
$(function () {
    getExamplesList();

    $('#hidden-card-deck').on('show.bs.collapse', function () {
        getQuestionaireList();
    })

    showHarvestingResources();

    console.log("script.js ready!");
});
