const sparql_url = "https://sparql.knowledgehub.nfdi4earth.de",
    sub_url = "/knowledge-graph/query?query=";


function createCards() {
    const cardContainer = document.querySelector("#container-cards");
    projects.forEach(function (project) {
        const card = document.createElement("div")
        const cardcol = document.createElement("div")
        card.classList.add("card")
        card.classList.add("mb-4")
        console.log(projects.length)
        cardcol.classList.add("d-flex");
        cardcol.classList.add("align-items-stretch");
        if (projects.length <= 2) {
            cardcol.classList.add("col-md-6");
        } else {
            cardcol.classList.add("col-md-4");
        }

        card.innerHTML = `
        <a href="${project.url}" class="project-link">
        <img class="card-img-top" src="${project.pic}" alt="Card image cap">
        </a>
            <div class="card-body d-flex flex-column position-relative">
                <h5 class="card-title font-weight-bold">${project.title}</h5>
                <p class="card-text">${project.details}</p>
                <a href="${project.url}" class="mt-auto">
                    <button class="btn btn-lg btn-block btn-primary">
                    Go there
                </button>
                </a>
            </div>
        </div>        `;
        cardcol.appendChild(card);
        cardContainer.appendChild(cardcol);

    });
}


function createNavbarRefs() {
    navbar = document.getElementById("navbar");
    projects.forEach(function (project) {
        if (project.items) {
            // Dropdow Menu
            const navInitialDropdown = document.createElement("li")
            navInitialDropdown.classList.add("nav-item")
            navInitialDropdown.classList.add("dropdown")
            navbar.appendChild(navInitialDropdown)
            const dropdownButton = document.createElement("div")
            dropdownButton.classList.add("dropdown-menu")
            dropdownButton.classList.add("dropdown-menu-right")
            dropdownButton.setAttribute("aria-labelledby", "navbarDopdownMenuLink")
            navInitialDropdown.appendChild(dropdownButton)
            const dropdownButtonLink = document.createElement("a")
            navInitialDropdown.appendChild(dropdownButtonLink)
            dropdownButtonLink.outerHTML = `<a class="nav-link dropdown-toggle" href="${project.url}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> ${project.title} </a>`
            // Dropdown Items
            project.items.forEach(function (item) {
                const dropdownItem = document.createElement("a");
                dropdownItem.classList.add("dropdown-item");
                dropdownItem.href = item.url
                dropdownItem.id = "navbar-button-" + item.name;
                dropdownItem.text = item.name
                dropdownButton.appendChild(dropdownItem);
            })

        } else {
            // Single Button
            const listItemButton = document.createElement("li")
            listItemButton.classList.add("nav-item")
            const singleButton = document.createElement("a");
            navbar.appendChild(singleButton);
            singleButton.outerHTML = `<a class="nav-link" href="${project.url}">${project.title}</a>`

        }
    })

}

createNavbarRefs();
createCards();
