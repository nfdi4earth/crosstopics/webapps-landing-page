var projects = [
    {
        "title": "Knowledge Hub",
        "pic": "image.png",
        "details": "Welcome to the NFDI4Earth Knowledge Hub, the central service that stores information on various research products from the Earth System Sciences (ESS).",
        "url": "https://knowledgehub.nfdi4earth.de/"
    },
    {
        "title": "Webapps",
        "pic": "App.png",
        "details": "This page hosts web applications that have been developed by the NFDI4Earth community.",
        "url": "https://webapps.nfdi4earth.de/",
        "items": [
            {
                "name": "Graph Based Visual Search Engine",
                "url": "https://vesa.webapps.nfdi4earth.de/"
            }
        ]
    }
]
